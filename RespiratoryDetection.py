"""
This module is intended to analyze respiratory signals and see how many breaths the user takes in a certain
time depending on the type of music they listen. Each audio file is 1 minute.

The outcome of this project is intended to build a table result that tells the user their Respiratory Rate
Activity vs. Music Type (e.g. Within 1 minute, you take 20 breaths while listening to R&B).

The required device is Lithic sensor. The module to run Lithic sensor is lithic.py

Sometimes, the Lithic sensor might not be able to detect number of breaths per minute very accurately. Hence, it
will return an approximate result.

"""

import asyncio
import json
from time import *
from lithic import Lithic
from scipy.signal import butter, sosfilt
import winsound
from MusicChoice import *
from datetime import datetime
import winsound
import pandas as pd


async def sprint11(lithic):
    # Connect to the sensor system
    print('Checking server connection')
    res = await lithic.connectToServer()

    # Quit program if connection is not established
    if res == "Server unreachable":
        print(res)
        raise SystemExit
    else:
        print('OK')
    str(input('Press ENTER to start the test: '))
    print('OK')

    # Output a message informing the user of the appropriate sensor module orientation.
    print("\n******** IMPORTANT INSTRUCTION. PLEASE READ CAREFULLY ********\n")
    print(
        'Please orient your Lithic sensor device so the rounded case of sensor is facing up to your chin, the LED'
        'light pointing away from you, and your Lithic sensor is hold firmly on your chest with your left hand.\n'
        '\nPlease sit up straight and still on your seat, and breathe normally for 1 minute during the recording time.\n'
        '\nEnsure that the sensor has a direct contact with your chest (without clothing layer) and the flat'
        ' side of the sensor is pressed firmly on your chest.\n')
    input('\nConfirm that you have read all the instructions by pressing any key: ')
    print("\n******** TEST BEGINS HERE ********\n")

    count_lst = []  # A list contains number of breath counts
    while True:
        try:
            # User prompt: User's full name
            user = str(input('\n\nEnter Full Name of Participant: '))
        except ValueError:
            print('ERROR: Name of user must be a string')
        else:
            if user.find(' ') == -1:
                print('ERROR: Name should include first and last name, separated by a space')
            else:
                break
    # User prompt: Tester's Full Name
    while True:
        try:
            tester = str(input('\n\nEnter Full Name of Tester: '))
        except ValueError:
            print('ERROR: Name of tester must be a string')
        else:
            if tester.find(' ') == -1:
                print('ERROR: Name should include first and last name, separated by a space')
            else:
                break

    # Begin the recording
    while True:
        count_lst = []

        for i in range(1, 6):

            print('\nRecording begins after BEEP')
            time.sleep(1)
            winsound.Beep(500, 1300)  # Beep sound at 500Hz for 1.3s
            await lithic.startRecording()
            print('\nDon\'t forget to breathe normally :)')

            # Start the audio files and record while they are playing
            if i == 1:  # First audio file
                metal()
                song_used = 'metal'
            elif i == 2:  # Second audio file
                RB()
                song_used = 'R&B'
            elif i == 3:  # Third audio file, and so forth with i = 4 and i = 5
                Jazz()
                song_used = 'Jazz'
            elif i == 4:
                classic()
                song_used = 'classical'
            else:
                Rap()
                song_used = 'rap'
                print("This is the last audio")

            # End recording and ask for recorded data
            res = await lithic.stopRecording()
            print("Recording stop.")
            file = open('data.txt', 'w')  # Opens file for writing, creates file if it does not exist
            file.write("timestamp,acc_x,acc_y,acc_z,gyro_x,gyro_y,gyro_z\n")
            # Initialize lists
            signal1 = []
            data = json.loads(res)
            file = open('data.txt', 'a')
            for frame in data:
                for sample in frame['samples']:
                    file.write(sample + "\n")
                    sample_list = sample.strip().split(',')
                    signal1.append(float(sample_list[4]))  # Grab the gyro's x-axis
            file.close()

            # Filter signal 1
            fs = 250
            fc = 20
            order = 10
            sos = butter(order, fc, 'low', False, 'sos', fs)
            signal1_filtered = sosfilt(sos, signal1).tolist()

            # 1 peak above threshold = 1 breath
            threshold = 2
            count_breath = 0  # Count the number of breaths during recording time
            breath_lst = []  # A list that contains all data points above threshold

            # Grab index of data points. We need to group data that classifies as 1 breath.
            for breath in signal1_filtered:
                if breath >= threshold:
                    breath_lst.append(breath)

            # Based on my observation of my own respiratory rate, each inhalation has around 350 data points.
            # I will equate 350 data points as 1 breath. This value can vary depends on the person.
            chunk = 350  # A chunk of 350 data points
            breath_cycle = [breath_lst[n:n + chunk] for n in range(0, len(breath_lst), chunk)]
            count_breath = len(breath_cycle)
            count_lst.append(count_breath)
            print("\nBreak for 30s to regulate your breathing again...")
            time.sleep(30)  # Between each audio file, I put a 30s rest so the user can regulate their breathing

        # The result will be appended to a table result in a file
        date = datetime.now().strftime('%d %b %Y')  # Get the current date
        t = datetime.now().strftime('%I:%M:%S %p')  # Get the current time
        music_type = ["Metal", "R&B", "Jazz", "Classical", "Rap"]
        print("\nHi, " + user + "! Here is your result")
        print("\nTest Date: " + str(date))
        print("\nTest Time: " + str(t))
        print("\nName of Tester: " + tester)
        print("\n***** Your Respiratory Rate vs. Music Type Results *****")
        df = pd.DataFrame(data=count_lst, index=music_type, columns=["Number of Breaths Taken"])  # Turn list into pandas' data frame for table
        print(df)
        print("\nYour result table will be saved in a separate file (Sprint 12)")

        # Ask for user input once the test is done
        user_input = str(input(
            "Would you like to perform another test?\nPress 'n' or 'q' to quit. Press other keys to continue: "))

        if user_input == 'q' or user_input == 'n':
            print("Thank you for taking the test. Goodbye :)")
            quit()
        else:
            continue


async def main():
    await sprint11(Lithic())


asyncio.run(main())
